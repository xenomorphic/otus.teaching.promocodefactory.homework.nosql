﻿using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using Otus.Teaching.Pcf.Administration.DataAccess.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Repositories
{
    public class EmployeesRepository : MongoRepository<Employee>
    {
        public EmployeesRepository(IMongoDbSettings settings)
            : base(settings, "Employees")
        {
        }
    }
}
